/* Copyright (c) 2021 OceanBase and/or its affiliates. All rights reserved.
miniob is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

//
// Created by Killtimer on 2023/10/31.
//

#include "sql/executor/drop_index_executor.h"

#include "session/session.h"
#include "common/log/log.h"
#include "storage/table/table.h"
#include "storage/index/index.h"
#include "sql/stmt/drop_index_stmt.h"
#include "event/sql_event.h"
#include "event/session_event.h"
#include "storage/db/db.h"

RC DropIndexExecutor::execute(SQLStageEvent *sql_event)
{
  RC rc = RC::SUCCESS;

  Stmt *stmt = sql_event->stmt();
  SessionEvent *session_event = sql_event->session_event();
  Session *session = session_event->session();
  ASSERT(stmt->type() == StmtType::DROP_INDEX, 
         "drop index executor can not run this command: %d", static_cast<int>(stmt->type()));

  DropIndexStmt *drop_index_stmt = static_cast<DropIndexStmt *>(stmt);

  SqlResult *sql_result = session_event->sql_result();

  Table *table = drop_index_stmt->table();
  Index *index = drop_index_stmt->index();

  if (table != nullptr && index != nullptr) {
    // TODO
    rc = RC::UNIMPLENMENT;
    sql_result->set_return_code(RC::UNIMPLENMENT);
  } else if (table == nullptr) {
    sql_result->set_return_code(RC::SCHEMA_TABLE_NOT_EXIST);
    sql_result->set_state_string("Table not exists");
  } else if (index == nullptr) {
    sql_result->set_return_code(RC::SCHEMA_INDEX_NOT_EXIST);
    sql_result->set_state_string("Index not exists");
  }

  return rc;
}