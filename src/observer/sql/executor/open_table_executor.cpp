/* Copyright (c) 2021 OceanBase and/or its affiliates. All rights reserved.
miniob is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

//
// Created by Killtimer on 2023/10/31.
//

#include "sql/executor/open_table_executor.h"

#include "session/session.h"
#include "common/log/log.h"
#include "storage/table/table.h"
#include "sql/stmt/open_table_stmt.h"
#include "event/sql_event.h"
#include "event/session_event.h"
#include "storage/db/db.h"

RC OpenTableExecutor::execute(SQLStageEvent *sql_event)
{
  RC rc = RC::SUCCESS;

  Stmt *stmt = sql_event->stmt();
  SessionEvent *session_event = sql_event->session_event();
  Session *session = session_event->session();
  ASSERT(stmt->type() == StmtType::OPEN_TABLE, 
         "open table executor can not run this command: %d", static_cast<int>(stmt->type()));

  OpenTableStmt *open_table_stmt = static_cast<OpenTableStmt *>(stmt);

  SqlResult *sql_result = session_event->sql_result();

  Table *table = open_table_stmt->table();

  if (table != nullptr) {
    if (table->can_use()) {
      rc = RC::AUTH_FAIL;
      sql_result->set_return_code(RC::AUTH_FAIL);
      sql_result->set_state_string("Table already opened");
    } else {
      if (table->update_password(open_table_stmt->password())) {
        table->open();
      } else {
        rc = RC::AUTH_FAIL;
        sql_result->set_return_code(RC::AUTH_FAIL);
        sql_result->set_state_string("Invalid password");
      }
    }
  } else {
    sql_result->set_return_code(RC::SCHEMA_TABLE_NOT_EXIST);
    sql_result->set_state_string("Table not exists");
  }

  return rc;
}