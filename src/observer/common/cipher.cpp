/* Copyright (c) 2021 OceanBase and/or its affiliates. All rights reserved.
miniob is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

//
// Created on 2023/11/05.
//

#include "common/cipher.h"
#include "common/hash.h"
#include <algorithm>
#include <cstring>
const unsigned char Cipher::Sbox[16][16] = {
	0xd6, 0x90, 0xe9, 0xfe, 0xcc, 0xe1, 0x3d, 0xb7, 0x16, 0xb6, 0x14, 0xc2, 0x28, 0xfb, 0x2c, 0x05,
	0x2b, 0x67, 0x9a, 0x76, 0x2a, 0xbe, 0x04, 0xc3, 0xaa, 0x44, 0x13, 0x26, 0x49, 0x86, 0x06, 0x99,
	0x9c, 0x42, 0x50, 0xf4, 0x91, 0xef, 0x98, 0x7a, 0x33, 0x54, 0x0b, 0x43, 0xed, 0xcf, 0xac, 0x62,
	0xe4, 0xb3, 0x1c, 0xa9, 0xc9, 0x08, 0xe8, 0x95, 0x80, 0xdf, 0x94, 0xfa, 0x75, 0x8f, 0x3f, 0xa6,
	0x47, 0x07, 0xa7, 0xfc, 0xf3, 0x73, 0x17, 0xba, 0x83, 0x59, 0x3c, 0x19, 0xe6, 0x85, 0x4f, 0xa8,
	0x68, 0x6b, 0x81, 0xb2, 0x71, 0x64, 0xda, 0x8b, 0xf8, 0xeb, 0x0f, 0x4b, 0x70, 0x56, 0x9d, 0x35,
	0x1e, 0x24, 0x0e, 0x5e, 0x63, 0x58, 0xd1, 0xa2, 0x25, 0x22, 0x7c, 0x3b, 0x01, 0x21, 0x78, 0x87,
	0xd4, 0x00, 0x46, 0x57, 0x9f, 0xd3, 0x27, 0x52, 0x4c, 0x36, 0x02, 0xe7, 0xa0, 0xc4, 0xc8, 0x9e,
	0xea, 0xbf, 0x8a, 0xd2, 0x40, 0xc7, 0x38, 0xb5, 0xa3, 0xf7, 0xf2, 0xce, 0xf9, 0x61, 0x15, 0xa1,
	0xe0, 0xae, 0x5d, 0xa4, 0x9b, 0x34, 0x1a, 0x55, 0xad, 0x93, 0x32, 0x30, 0xf5, 0x8c, 0xb1, 0xe3,
	0x1d, 0xf6, 0xe2, 0x2e, 0x82, 0x66, 0xca, 0x60, 0xc0, 0x29, 0x23, 0xab, 0x0d, 0x53, 0x4e, 0x6f,
	0xd5, 0xdb, 0x37, 0x45, 0xde, 0xfd, 0x8e, 0x2f, 0x03, 0xff, 0x6a, 0x72, 0x6d, 0x6c, 0x5b, 0x51,
	0x8d, 0x1b, 0xaf, 0x92, 0xbb, 0xdd, 0xbc, 0x7f, 0x11, 0xd9, 0x5c, 0x41, 0x1f, 0x10, 0x5a, 0xd8,
	0x0a, 0xc1, 0x31, 0x88, 0xa5, 0xcd, 0x7b, 0xbd, 0x2d, 0x74, 0xd0, 0x12, 0xb8, 0xe5, 0xb4, 0xb0,
	0x89, 0x69, 0x97, 0x4a, 0x0c, 0x96, 0x77, 0x7e, 0x65, 0xb9, 0xf1, 0x09, 0xc5, 0x6e, 0xc6, 0x84,
	0x18, 0xf0, 0x7d, 0xec, 0x3a, 0xdc, 0x4d, 0x20, 0x79, 0xee, 0x5f, 0x3e, 0xd7, 0xcb, 0x39, 0x48
};
const unsigned int Cipher::FK[4] = {0XA3B1BAC6,0X56AA3350,0X677D9197,0XB27022DC};
const unsigned int Cipher::CK[32] = {
	0x00070e15, 0x1c232a31, 0x383f464d, 0x545b6269, 0x70777e85, 0x8c939aa1, 0xa8afb6bd, 0xc4cbd2d9,
	0xe0e7eef5, 0xfc030a11, 0x181f262d, 0x343b4249, 0x50575e65, 0x6c737a81, 0x888f969d, 0xa4abb2b9,
	0xc0c7ced5, 0xdce3eaf1, 0xf8ff060d, 0x141b2229, 0x30373e45, 0x4c535a61, 0x686f767d, 0x848b9299,
	0xa0a7aeb5, 0xbcc3cad1, 0xd8dfe6ed, 0xf4fb0209, 0x10171e25, 0x2c333a41, 0x484f565d, 0x646b7279
};


Cipher::Cipher(const std::string key)
{
  static_assert(sizeof (char) == sizeof (uint8_t), "char value is not 8-bit length!");
  hash_calc(reinterpret_cast<const uint8_t *>(key.data()), key.size(), this->key);
}

Cipher::Cipher(const uint8_t key[16])
{
  std::copy_n(key, 16, this->key);
}
//轮函数F中合成置换T
unsigned int Cipher::T(unsigned int X) const{
	unsigned char a3 = X & 0x00000ff;
	unsigned char a2 = (X & 0x0000ff00) >> 8;
	unsigned char a1 = (X & 0x00ff0000) >> 16;
	unsigned char a0 = (X & 0xff000000) >> 24;
	unsigned char b3 = Sbox[(a3 & 0xf0) >> 4][a3 & 0x0f];
	unsigned char b2 = Sbox[(a2 & 0xf0) >> 4][a2 & 0x0f];
	unsigned char b1 = Sbox[(a1 & 0xf0) >> 4][a1 & 0x0f];
	unsigned char b0 = Sbox[(a0 & 0xf0) >> 4][a0 & 0x0f];

	unsigned int B = (b0 << 24) | (b1 << 16) | (b2 << 8) | (b3);
	unsigned int C = B ^ rol(B, 2) ^ rol(B, 10) ^ rol(B, 18) ^ rol(B, 24);
	return C;
}
void Cipher::uint8ToUint(const uint8_t input[4], unsigned int& output) const{
		output = (static_cast<unsigned int>(input[0]) << 24) |
			(static_cast<unsigned int>(input[1]) << 16) |
			(static_cast<unsigned int>(input[2]) << 8) |
			static_cast<unsigned int>(input[3]);
}
void Cipher::uintToUint8(const unsigned int& input, uint8_t output[4]) const{
		output[0] = (input >> 24) & 0xFF;
		output[1] = (input >> 16) & 0xFF;
		output[2] = (input >> 8) & 0xFF;
		output[3] = input & 0xFF;
}
void Cipher::selfIncrease(uint8_t counter[16]) const {
	bool flag = true;
	while(flag){
		for(int i = 15; i >= 0; i--){
			counter[i]++;
			if (counter[i] != 0){
				flag = false;
				break;
			}
		}
	}
}
//密钥扩展算法中的合成置换T'
unsigned int Cipher::T2(unsigned int X) const{
	unsigned char a3 = X & 0x00000ff;
	unsigned char a2 = (X & 0x0000ff00) >> 8;
	unsigned char a1 = (X & 0x00ff0000) >> 16;
	unsigned char a0 = (X & 0xff000000) >> 24;
	unsigned char b3 = Sbox[(a3 & 0xf0) >> 4][a3 & 0x0f];
	unsigned char b2 = Sbox[(a2 & 0xf0) >> 4][a2 & 0x0f];
	unsigned char b1 = Sbox[(a1 & 0xf0) >> 4][a1 & 0x0f];
	unsigned char b0 = Sbox[(a0 & 0xf0) >> 4][a0 & 0x0f];

	unsigned int B = (b0 << 24) | (b1 << 16) | (b2 << 8) | (b3);
	unsigned int C = B ^ rol(B, 13) ^ rol(B, 23);
	return C;
}
void Cipher::encipherFor4Word(const uint8_t input[16], const uint8_t key[16],uint8_t output[16]) const
{
  unsigned int rk[32] = { 0x0 };
	unsigned int X[36] = { 0x0 };
	unsigned int K[36] = { 0x0 };
	unsigned int keyForWord[4] = { 0x0 };
	for (int i = 0; i < 4; i++) {
		uint8ToUint(&input[4*i], X[i]);
		uint8ToUint(&key[4*i], keyForWord[i]);
		K[i] = keyForWord[i] ^ FK[i];
	}
	for (int i = 0; i < 32; i++) {
		K[i + 4] = K[i] ^ T2(K[i + 1] ^ K[i + 2] ^ K[i + 3] ^ CK[i]);
		rk[i] = K[i + 4];
	}
	for (int i = 0; i < 32; i++) {
		X[i + 4] = X[i] ^ T(X[i + 1] ^ X[i + 2] ^ X[i + 3] ^ rk[i]);
	}
	for (int i = 0; i < 4; i++) {
		uintToUint8(X[35 - i], &output[4 * i]);
	}
}
void Cipher::decipherFor4Word(const uint8_t input[16], const uint8_t key[16],uint8_t output[16]) const
{
  unsigned int rk[32] = { 0x0 };
	unsigned int X[36] = { 0x0 };
	unsigned int K[36] = { 0x0 };
	unsigned int keyForWord[4] = { 0x0 };
	for (int i = 0; i < 4; i++) {
		uint8ToUint(&input[4 * i], X[i]);
		uint8ToUint(&key[4 * i], keyForWord[i]);
		K[i] = keyForWord[i] ^ FK[i];
	}
	for (int i = 0; i < 32; i++) {
		K[i + 4] = K[i] ^ T2(K[i + 1] ^ K[i + 2] ^ K[i + 3] ^ CK[i]);
		rk[i] = K[i + 4];
	}
	for (int i = 0; i < 32; i++) {
		X[i + 4] = X[i] ^ T(X[i + 1] ^ X[i + 2] ^ X[i + 3] ^ rk[31-i]);
	}
	for (int i = 0; i < 4; i++) {
		uintToUint8(X[35 - i], &output[4 * i]);
	}
}
void Cipher::encrypt(const uint8_t data[16], uint8_t output[16]) const
{
  // TODO: 实现SM4加密
  encipherFor4Word(data, key, output);
  // 使用异或实现假的算法，便于调试
  /*for (int i = 0; i < 16; ++i) {
    output[i] = data[i] ^ key[i];
  }*/
}

void Cipher::decrypt(const uint8_t data[16], uint8_t output[16]) const
{
  // TODO: 实现SM4解密

  // 使用异或实现假的算法，便于调试
  decipherFor4Word(data, key, output);
  
}

void Cipher::encrypt_ctr(const uint8_t nonce[16], const uint8_t *data, std::size_t len, uint8_t *output) const
{
  // TODO: 使用CTR模式进行流式加密
	uint8_t keyStream[16] = { 0x0 };
	uint8_t counter[16] = { 0x0 };
	int n = len / 16;
	int m = len % 16;
	for (int i = 0; i < 16; i++)
		counter[i] = nonce[i];
	for (int i = 0; i < n; i++) {
		encipherFor4Word(counter, key, keyStream);
		selfIncrease(counter);
		for (int j = 0; j < 16; j++)
			output[i * 16 + j] = data[i * 16 + j] ^ keyStream[j];
	}
	encipherFor4Word(counter, key, keyStream);
	selfIncrease(counter);
	for (int j = 0; j < m; j++)
		output[n * 16 + j] = data[n * 16 + j] ^ keyStream[j];

	// 使用异或实现假的算法，便于调试
	/*for (size_t i = 0; i < len; ++i) {
	  output[i] = data[i] ^ nonce[i & 15] ^ key[i & 15];
	}*/
}
