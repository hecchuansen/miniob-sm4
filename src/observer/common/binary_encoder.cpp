/* Copyright (c) 2021 OceanBase and/or its affiliates. All rights reserved.
miniob is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

//
// Created on 2023/11/05.
//

#include "common/binary_encoder.h"

std::string BinaryEncoder::encode(const uint8_t *data, size_t len)
{
  std::string result;
  result.reserve(len * 2);
  for (size_t i = 0; i < len; ++i)
  {
    uint8_t high = data[i] >> 4;
    uint8_t low = data[i] & 0x0f;
    result.push_back(high < 10 ? high + '0' : high - 10 + 'A');
    result.push_back(low < 10 ? low + '0' : low - 10 + 'A');
  }
  return result;
}

bool BinaryEncoder::try_decode(const std::string &str, std::vector<uint8_t> &result)
{
  result.clear();
  if (str.length() % 2 != 0) {
      return false;
  }
  result.reserve(str.length() / 2);
  auto hex2v = [](char c) {
      if ('0' <= c && c <= '9') {
          return c - '0';
      } else if ('A' <= c && c <= 'F') {
          return c - 'A' + 10;
      } else if ('a' <= c && c <= 'f') {
          return c - 'a' + 10;
      } else {
          return -1;
      }
  };
  size_t i;
  for (i = 0; i + 1 < str.length(); i += 2) {
      int h = hex2v(str[i]), l = hex2v(str[i + 1]);
      if (h == -1 || l == -1) {
          return false;
      }
      result.emplace_back(h << 4 | l);
  }
  return i == str.length();
}
