/* Copyright (c) 2021 OceanBase and/or its affiliates. All rights reserved.
miniob is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

//
// Created on 2023/11/07.
//

#pragma once

#include <cstdint>
#include <unistd.h>

/**
 * @brief 密码学安全的随机数发生器
 * @details 读取/dev/random实现
 */
class SecRand
{
public:
  SecRand();
  ~SecRand();

  /**
   * @brief 生成指定数量的随机字节
   * @details 如果IO操作失败，则使用标准库的伪随机数
   * 
   * @param data    输出缓冲区
   * @param len     缓冲区长度
  */
  void generate(uint8_t *data, size_t len);

  /**
   * @brief 生成指定数量的随机字节
   * @details 如果IO操作失败，则使用标准库的伪随机数
   * 
   * @param data    输出缓冲区
   * @param len     缓冲区长度
  */
  static void randbytes(uint8_t *data, size_t len);

private:
  int fd;
  static SecRand instance;
};

