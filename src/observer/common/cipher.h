/* Copyright (c) 2021 OceanBase and/or its affiliates. All rights reserved.
miniob is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

//
// Created on 2023/11/05.
//

#pragma once

#include "common/hash.h"

#include <cstdint>
#include <string>

/**
 * @brief 加密和解密的密钥
 * @details 此密钥使用SM4算法
 */
class Cipher
{
public:
  Cipher() = default;
  explicit Cipher(const std::string key);
  explicit Cipher(const uint8_t key[16]);
  bool operator==(const Cipher &rhs) const
  { return std::equal(std::begin(key), std::end(key), std::begin(rhs.key)); }

  const uint8_t *get_key() const { return key; }
  void export_hash(uint8_t hash[16]) const { hash_calc(key, 16, hash); }

  /**
   * @brief SM4加密
   * @details 实现单个数据块的加密，data和output缓冲区可重叠。
   * 
   * @param data    输入数据
   * @param output  输出缓冲区
  */
  void encrypt(const uint8_t data[16], uint8_t output[16]) const;

  /**
   * @brief SM4加密，使用CTR模式（流式加密）
   * @details 实现任意长度数据的加密，data和output缓冲区可重叠。
   *
   * @param nonce   随机数
   * @param data    输入数据
   * @param len     输入数据长度
   * @param output  输出缓冲区
  */
  void encrypt_ctr(const uint8_t nonce[16], const uint8_t *data, std::size_t len, uint8_t *output) const;

  /**
   * @brief SM4解密
   * @details 实现单个数据块的解密，此处暂时没有使用。data和output缓冲区可重叠。
   *
   * @param data    输入数据
   * @param output  输出缓冲区
  */
  void decrypt(const uint8_t data[16], uint8_t output[16]) const;

  /**
   * @brief SM4解密，使用CTR模式（流式加密）
   * @details 实现任意长度数据的解密，由于是流式加密，因此加密函数就是解密函数。data和output缓冲区可重叠。
   *
   * @param nonce   随机数
   * @param data    输入数据
   * @param len     输入数据长度
   * @param output  输出缓冲区
  */
  void decrypt_ctr(const uint8_t nonce[16], const uint8_t *data, std::size_t len, uint8_t *output) const
  { return encrypt_ctr(nonce, data, len, output); }
  unsigned int rol(unsigned int X, unsigned short int n) const{return (X >> (sizeof(unsigned int) * 8 - n) | (X << n));}
  //定义轮函数F中合成置换T
  unsigned int T(unsigned int X) const;
  //定义密钥扩展算法中的合成置换T'
  unsigned int T2(unsigned int X) const;
  void encipherFor4Word(const uint8_t input[16], const uint8_t key[16],uint8_t output[16]) const;
  void decipherFor4Word(const uint8_t input[16], const uint8_t key[16],uint8_t output[16]) const;
  void uint8ToUint(const uint8_t input[4], unsigned int& output) const;
  void uintToUint8(const unsigned int& input, uint8_t output[4]) const;
  void selfIncrease(uint8_t counter[16]) const;

private:
  uint8_t key[16];
  static const unsigned char Sbox[16][16];

/*------------------------------------- Definition of  Parameter --------------------------------*/
static const unsigned int FK[4];
static const unsigned int CK[32];
};