/* Copyright (c) 2021 OceanBase and/or its affiliates. All rights reserved.
miniob is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

//
// Created on 2023/11/05.
//

#pragma once

#include <cstdint>
#include <string>
#include <vector>

/**
 * @brief 将二进制数据与字符串进行互转
 */
struct BinaryEncoder
{
  /**
   * @brief 将二进制数据转换为字符串
   * 
   * @param data    输入数据
   * @param len     输入数据长度
  */
  static std::string encode(const uint8_t *data, size_t len);

  /**
   * @brief 尝试将字符串转换为二进制数据
   *
   * @param str     输入字符串
   * @param result  输出结果
  */
  static bool try_decode(const std::string &str, std::vector<uint8_t> &result);
};

