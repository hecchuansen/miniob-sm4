/* Copyright (c) 2021 OceanBase and/or its affiliates. All rights reserved.
miniob is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

//
// Created on 2023/11/07.
//

#include "common/rand.h"
#include <fcntl.h>
#include <cstdlib>
#include <ctime>

SecRand SecRand::instance;

SecRand::SecRand() {
  fd = open("/dev/random", O_RDONLY);
  if (fd < 0) {
    srand(time(nullptr));
  }
}

SecRand::~SecRand() {
  if (fd > 0) {
    close(fd);
  }
}

void SecRand::generate(uint8_t *data, size_t len) {
  if (fd > 0) {
    if (read(fd, data, len) == len) {
      return;
    }
  }
  for (size_t i = 0; i < len; ++i) {
    data[i] = rand() & 255;
  }
}

void SecRand::randbytes(uint8_t *data, size_t len) {
  return SecRand::instance.generate(data, len);
}